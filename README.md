
# Programming the Wahoo ELEMNT Bolt

Here i will collect various things i've discovered about programming the Wahoo
ELEMNT Bolt. This is very much a work in progress. The main outputs of
my faffing are

* [BoltOn - Android file manager for the Bolt][bolton]
* [A virtual pet app for the Bolt][virtlpt]
* [Python script implementing a wide-range of bluetooth interactions with Bolt][boltbt]

## On Device Apps

* [Hello World for the Bolt][helloworld] -- a guide to your first app
* [Blinking Lights and Other Revelations][blinking-lights] -- button
  presses, buzzer sounds, and LED control.

## Connecting via Bluetooth

* [BoltBT][bt-begin] -- the bluetooth protocol is well-documented in the
  documentation for BoltBT.

## Reverse Engineering

* [Reverse Engineering](topics/reverse-engineering.md) -- tools / tips,
  both for on device apps and bluetooth.

## Related Repositories

* [BoltOn - Android file manager for the Bolt][bolton]
* [A virtual pet app for the Bolt][virtlpt]
* [Python script for bluetooth interaction with Bolt][boltbt]
* [Creating your own map files][bolt-maps-creator]
* [Embedding simple turns in a GPX file][gpx-turns]


[helloworld]: topics/helloworld.md
[blinking-lights]: topics/blinking-lights.md
[bolt-maps-creator]: https://gitlab.com/Hague/bolt-maps-creator
[virtlpt]: https://gitlab.com/Hague/virtlpt
[gpx-turns]: https://gitlab.com/Hague/gpx-turns
[boltbt]: https://gitlab.com/Hague/boltbt
[bolton]: https://gitlab.com/Hague/bolton
[bt-begin]: https://hague.gitlab.io/boltbt/
