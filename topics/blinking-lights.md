
# Blinking Lights and Other Revelations

This is a guide to controlling the LEDs and buzzer sounds on the Wahoo
ELEMNT Bolt. We assume you're happy with programming apps already. If
not, see [Hello World](helloworld.md).

## Contents

1. [Button Presses](#button-presses)
2. [Buzzer Sounds](#buzzer-sounds)
3. [LED Patterns](#led-patterns)
4. [Backlight](#backlight)


## Button Presses

Apps can receive button press notifications by registering to be
notified of the following intents

    com.wahoofitness.bolt.buttons.<button>.<action>

where `<button>` is one of

* power_button
* left_button
* center_button
* right_button
* up_button
* down_button

and `<action>` is one of

* up
* down
* double_pressed
* long_pressed

For example, a long press of the center button would be

    com.wahoofitness.bolt.buttons.center_button.long_pressed

We will assume you are familiar with receiving these kind of intents. If
not, see [Hello World](helloworld.md) for an example.

## Buzzer Sounds

Buzzer sounds are also controlled by intents. The most important intent
is

    com.wahoofitness.bolt.service.BBuzzerManager.PATTERN

Other available intents are

    com.wahoofitness.bolt.service.BBuzzerManager.MARIO
    com.wahoofitness.bolt.service.BBuzzerManager.MUTE
    com.wahoofitness.bolt.service.BBuzzerManager.SHH
    com.wahoofitness.bolt.service.BBuzzerManager.UNMUTE

The PATTERN intent plays a customizable tune. The tune is specified by a
"pattern" extra in the intent. For example, borrowing the "Triumph"
music from the Bolt

    val TRIUMPH = "B#1046 S#50 B#0 S#1 B#1318 S#50 B#0 S#1 B#1567 S#50 B#0 S#1 B#2093 S#80 B#0 S#1"
    Intent().also { intent ->
        intent.setAction("com.wahoofitness.bolt.service.BBuzzerManager.PATTERN")
        intent.putExtra("pattern", TRIUMPH)
        sendBroadcast(intent)
    }

The pattern string is written in a mini-language for buzzer music. The
Triumph music begins with the command "B#1046" followed by "S#50" and so
on. Commands i'm aware of are

* `B#<tone>` -- starts playing a sound where `<tone>` is a number
  representing the pitch of the note,
* `S#<duration>` -- pauses for `<duration>` amount of time. If the
  buzzer is playing a sound, it will continue to play that sound during
  the pause.
* `REPEAT` -- repeat the music
* `REPEAT#<num>` -- repeat the music `<num>` times.

## LED Patterns

The LEDs are controlled by intents, similar to the buzzer. The main
intents are

    com.wahoofitness.bolt.service.BLedManager.PATTERN
    com.wahoofitness.bolt.service.BLedManager.CLRALL

Other available intents are

    com.wahoofitness.bolt.service.BLedManager.CHASEB
    com.wahoofitness.bolt.service.BLedManager.CHASEG
    com.wahoofitness.bolt.service.BLedManager.CHASER
    com.wahoofitness.bolt.service.BLedManager.HWMONITOR
    com.wahoofitness.bolt.service.BLedManager.OVERRIDE_OFF
    com.wahoofitness.bolt.service.BLedManager.OVERRIDE_ON
    com.wahoofitness.bolt.service.BLedManager.PWRDWN
    com.wahoofitness.bolt.service.BLedManager.ZONE

The intent PATTERN will begin the LEDs playing a sequence of light
commands. CLRALL will stop the sequence.

Each PATTERN intent should be sent with a string extra called "pattern".
For example, borrowing a pattern from [VIRTLPT][virtlpt]

    val LED_PLAY = "T3#00ff00 S#250 T3#0 T2#00ff00 S#250 T2#0 T1#00ff00 S#250 T1#0 T0#00ff00 S#250 T0#0 T3#00ff00 S#250 T3#0 T4#00ff00 S#250 T4#0 T5#00ff00 S#250 T5#0 T6#00ff00 S#250 T6#0 REPEAT"
    Intent().also { intent ->
        intent.setAction("com.wahoofitness.bolt.service.BLedManager.PATTERN")
        intent.putExtra("pattern", LED_PLAY)
        sendBroadcast(intent)
    }

The LED pattern is a mini-language for controlling the LEDs at the top
of the device. There are seven LEDs named T0, T1, T2, T3, T4, T5, and
T6. Each pattern should be a series of instructions from

* `T<n>#<color>` -- where `<n>` is the LED number (0-6) and `<color>` is the
  hex code of the color to set the LED to (color 0 is off),
* `S#<duration>` -- pause for `<duration>`, leaving the LEDs unchanged,
* `REPEAT` -- repeat the sequence of instructions ad infinitum,
* `REPEAT#<n>` -- i haven't tested this, but i assume it might be
  possible (see [Buzzer Sounds](#buzzer-sounds)).

For example, to turn the middle LED on, with a green light, for 250
milliseconds before turning it off, the pattern should be

    T3#00ff00 S#250 T3#0

To alternate between the leftmost and rightmost LEDs in color blue, use

    T0#0000ff T6#0 S#250 T0#0 T6#0000ff S#250 REPEAT

Note, you have to remember to turn LEDs off as well as on.

## Backlight

I haven't figured out how to get the backlight working. There exist the
intents

    com.wahoofitness.bolt.service.sys.BBacklightManager.OVERRIDE_OFF
    com.wahoofitness.bolt.service.sys.BBacklightManager.OVERRIDE_ON

where OVERRIDE_ON takes the extras

* lowThresholdLux : Int
* highThresholdLux : Int
* lowAutoBrightness : Int
* highAutoBrightness : Int

but i don't know what to do with these.

[virtlpt]: https://gitlab.com/Hague/virtlpt

