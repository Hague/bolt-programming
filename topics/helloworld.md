
# Hello World - Your First ELEMNT Bolt App

Pleasingly, if you're happy with Android programming, it is pretty easy
to write apps for the ELEMNT Bolt. It runs a version of Android.

This tutorial assumes that you're happy writing Android apps and using
ADB.


## Step 1:  Get ADB Access

First connect your Bolt to your computer via USB. Then you can [unlock
ADB][joshua] on the ELEMNT Bolt by pressing the "Power" and "Up" buttons
at the same time. Perhaps do it a couple of times just to be sure. You
can verify that it worked by running

    $ adb devices

You should see something like

    List of devices attached
    5P85LBFMZDWG8PSG    device

If you see "unauthorized", try pressing "Power" and "Up" a few more
times, and run

    $ adb kill-server

before trying again.

## Step 2: Celebrate

Once you have ADB access, you might as well celebrate your first step
by running

    $ adb shell am broadcast -a com.wahoofitness.bolt.service.BBuzzerManager.MARIO

and hearing it sing.

## Step 3: Installing and Running A Basic Hello World

You can install any Android app in the usual way with ADB

    $ adb install any.apk

This means you can install any Hello World. One such example is
[here][helloworld].

Once installed you have to run the app. You can do this with ADB again.
Supposing you downloaded the [example helloworld][helloworld], you can
install it by running

    $ ./gradlew installDebug

The app is
com.matt.helloworld and the main activity is
com.matt.helloworld.MainActivity. To start the app, run

    $ adb shell am start -n com.matt.helloworld/com.matt.helloworld.MainActivity

You should see your hello world app appear over the Bolt app.

![Hello World](img/helloworld.png)

## Step 4: Stopping and Uninstalling The App

At this point you're probably more than well aware that programming the
Bolt isn't quite the same as normal Android programming: you don't have
a nice launcher tray in which you can find, run, and remove your app.
You also can't stop it easily, or interact via a touchscreen. This is
where you need to start to know a bit more about the Bolt.

But first, you can stop the app with

    $ adb shell am force-stop com.matt.helloworld

and you can uninstall it with

    $ adb uninstall com.matt.helloworld

## Step 5: Getting to Know The App

Before we discuss particulars of the Bolt, it's worth taking a moment to
look at the main components of the Hello World app. These are all basic
for an Android programmer, but we will adapt them in the following
steps, so it's good to have them fresh in your head.

First is the AndroidManifest.xml, where we specify the activities and
entry points of the app. The Hello World manifest registers the
MainActivity of the application in the usual way.

    <manifest
        xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.matt.helloworld">

        <application
            android:label="Hello World"
            android:theme="@style/AppTheme"
            android:allowBackup="false">

            <activity android:name=".MainActivity">
                <intent-filter>
                    <action android:name="android.intent.action.MAIN"/>
                    <action android:name="android.intent.action.VIEW"/>
                    <category android:name="android.intent.category.LAUNCHER"/>
                </intent-filter>
            </activity>

        </application>
    </manifest>

Next is the MainActivity code. I am using Kotlin, so MainActivity.kt is
as follows. It overrides onCreate to set set the main layout.

    package com.matt.helloworld

    import android.os.Bundle
    import android.view.View
    import androidx.appcompat.app.AppCompatActivity

    public class MainActivity : AppCompatActivity() {

        override protected fun onCreate(savedInstanceState : Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
        }

    }

## Step 6: Starting the App Without ADB

The first problem is how to start the app without an icon to click on.
The app wouldn't be much use if you needed ADB to start it. Fortunately
we can register the app to start whenever the Power button is double
clicked. In fact, we can tie the start of the app to any button action.

Button actions are described in full [elsewhere][buttons]. For now, we
just need to know that a double click of the Power button is advertised
via the intent

    com.wahoofitness.bolt.buttons.power_button.double_pressed

We will declare in our manifest a receiver of this event.  This receiver
will then start our app. Add a receiver to the application section of
our AndroidManifest.xml

    <manifest
        xmlns:android="http://schemas.android.com/apk/res/android"
        package="com.matt.helloworld">

        <application
            android:label="Hello World"
            android:theme="@style/AppTheme"
            android:allowBackup="false">

            <!-- ... -->

            <receiver android:name=".StartAppReceiver">
                <intent-filter >
                    <action android:name="com.wahoofitness.bolt.buttons.power_button.double_pressed"/>
                </intent-filter>
            </receiver>

        </application>
    </manifest>

Now, whenever the Power button is double clicked, the class
StartAppReceiver will be notified. We of course need to define what that
is. Create the file
app/src/main/kotlin/com/matt/helloworld/StartAppReceiver.kt and enter

    package com.matt.helloworld

    import android.content.BroadcastReceiver
    import android.content.Context
    import android.content.Intent

    public class StartAppReceiver : BroadcastReceiver() {

        override public fun onReceive(context : Context,
                                      intext : Intent) {
            val i = Intent(context, MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);
        }

    }

This code says that onReceive-ing the Power button intent we requested
in the manifest, a new intent should be fired that asks to start the
MainActivity class, which is the main activity of our app.

Run

    $ ./gradlew installDebug

to reinstall the app. You will need to run the app manually at least
once using

    $ adb shell am start -n com.matt.helloworld/com.matt.helloworld.MainActivity

Afterwards, when you double-click the Power button, the app should start
up. This persists through reboots. This behaviour is a [security feature
since Android 3.1][receiving]. You will also need to run the above
command if your force-stop your app via ADB.

## Step 7: Stopping the App Without ADB

The app still has the problem that once it's open you can't get out of
it. We will listen for a single click of the Power button. When one
occurs, we'll set the app to close.

To register to be notified of the Power button clicks, we need first to
create a broadcast receiver in the MainActivity class. This time we want
the intent

    com.wahoofitness.bolt.buttons.power_button.up

Remember also to import the right classes. We import IntentFilter too
because we'll need that later.

    ...
    import android.content.BroadcastReceiver
    import android.content.Context
    import android.content.Intent
    import android.content.IntentFilter

    public class MainActivity : AppCompatActivity() {

        private val POWER_UP = "com.wahoofitness.bolt.buttons.power_button.up"

        val br : BroadcastReceiver = object : BroadcastReceiver() {
            override fun onReceive(context : Context, intent : Intent) {
                when (intent.action) {
                    POWER_UP -> finishAndRemoveTask()
                }
            }
        }

        ...

The onReceive method of this receiver will check the action associated
with the intent. If it matches the Power button click

    com.wahoofitness.bolt.buttons.power_button.up

it will call the in-built android method finishAndRemoveTask which
closes down the activity (and returns you to the main Bolt app).

Just creating this receiver isn't enough: we need also to ask Android to
let us know when the Power button has been pressed. To do this, we add
an onResume method to the MainActivity that registers the receiver with
Android

    override protected fun onResume() {
        super.onResume()

        val filter = IntentFilter()
        filter.addAction(POWER_UP)

        registerReceiver(br, filter)
    }

This method creates an IntentFilter to tell Android we want to hear
about the Power Up events, and registers our BroadcastReceiver as the
object responsible for dealing with it.

Finally, we can clean up after ourselves if the app is paused by
unregistering the receiver

    override protected fun onPause() {
        super.onPause()
        unregisterReceiver(br)
    }

## Step 8: Install, Test, and Go Forth

You can now install the app

    $ ./gradlew installDebug

and hopefully start and stop it with the power button. 


## A Complete Implementation

A completed implementation of this tutorial is available
[here][helloworld-bolt].

## Going Further

It might not surprise you to know the following intents also work

    com.wahoofitness.bolt.buttons.left_button.up
    com.wahoofitness.bolt.buttons.center_button.up
    com.wahoofitness.bolt.buttons.right_button.up
    com.wahoofitness.bolt.buttons.up_button.up
    com.wahoofitness.bolt.buttons.down_button.up

To make the LEDs flash and the device bleep see [Blinking Lights and
Other Revelations][blinking-lights].

[joshua]: https://joshua0.dreamwidth.org/66991.html
[helloworld]: https://gitlab.com/Hague/android-helloworld
[helloworld-bolt]: https://gitlab.com/Hague/helloworld-bolt
[receiving]: https://commonsware.com/blog/2011/07/13/boot-completed-regression-confirmed.html
[buttons]: blinking-lights.md#button-presses
[blinking-lights]: blinking-lights.md
