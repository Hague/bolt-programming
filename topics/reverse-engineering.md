
# Reverse Engineering

A few hints on how to figure out what the Bolt is doing.

## Contents

1. [APK Decompilation](#apk-decompilation)
2. [Bluetooth Snooping](#bluetooth-snooping)
3. [ADB Logcat](#adb-logcat)
4. [Shared Preferences](#shared-preferences)

## APK Decompilation

The APK of the apps the Bolt uses can be pulled from the device and
decompiled. This can be useful for searching around to find the values
of names / constants that are important.

First connect the device by USB and [turn on ADB][joshua] by pressing
"Power" and "Up" simultaneously. Then get the APK of the Bolt app and
save it as "base.apk":

    $ adb pull /data/app/com.wahoofitness.bolt-2/base.apk bolt.apk

Once you have an APK you can decompile it with [APKTool][apktool]:

    $ apktool d bolt.apk

You can then snoop around to see what you find. For example, by
searching the file names, you can find the BBuzzerManager$1 class,
which contains a list of buzzer intents, including

    com.wahoofitness.bolt.service.BBuzzerManager.MARIO

## Bluetooth Snooping

To decode the bluetooth protocol it's useful to be able to monitor the
official Companion App.

The steps for getting bluetooth logs on Android 10 are as follows. Some
variation in process has been reported online, so you may have to search
for your own device / version of Android. [This page][androiddebugging]
might help, or might be more confusing!

0. Enable developer options by pressing "build number" in the settings 7
   times.
1. In developer options, set bluetooth snooping to "Enable".
2. Toggle bluetooth off/on (perhaps reboot).
3. Connect with app and do something.
4. Connect phone to computer and get ADB access.
5. On your computer, run "adb shell dumpsys bluetooth_manager".
6. Then, run "adb bugreport anewbugreport".
7. Unzip anewbugreport.zip.
8. Find the bluetooth log in FS/data/misc/bluetooth/logs/...
9. Open it with [Wireshark][wireshark].

## ADB Logcat

First connect the device by USB and [turn on ADB][joshua] by pressing
"Power" and "Up" simultaneously. Then run

    $ adb logcat

The device is very chatty so it can be hard to pull out pertinent
information. You can find a few things, e.g. figuring out button
intents.

    I/WahooCustormization(  576): actioncom.wahoofitness.bolt.buttons.power_button.up

## Shared Preferences

You can browse the shared preferences of the bolt application. Connect
via ADB as above. The shared preference files are under

    /data/user/0/com.wahoofitness.bolt/shared_prefs/

[joshua]: https://joshua0.dreamwidth.org/66991.html
[apktool]: https://ibotpeaches.github.io/Apktool/
[wireshark]: https://www.wireshark.org/
[androiddebugging]: https://source.android.com/devices/bluetooth/verifying_debugging
